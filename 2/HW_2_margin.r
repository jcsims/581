# Returns a function that can be used to classify an observation
# Based on the maximum-margin classifier model, algorithm details taken from
# Knowledge Discovery with Support Vector Machines, pp 73-86
mar.model <- function(dframe, q) {
  # Simple sanity check for the passed data
  if (!is.data.frame(dframe)) {
    stop("This functions accepts only data frames.")
  }

  r <- norm.vec(max.vector(dframe[,1:ncol(dframe)-1]))
  i.mat <- diag(ncol(dframe)-1)
  zero <- rep(0,ncol(dframe)-1)
  w.star <- NULL

  # Construct X
  X <- dframe
  for (i in 1:nrow(dframe)) {
    X[i,] <- X[i,ncol(X)] * X[i,]
  }
  X <- X[,1:ncol(X)-1]
  X <- t(X)

  for (b in -q:q) {
    # Construct c
    c <- dframe[,ncol(dframe)]
    c <- c * b + 1

    # Solve using the QP solver
    w <- my.solve.QP(i.mat,zero,X,c)

    if( (w$status == 0 && is.null(w.star)) ||
        (w$status == 0 && norm.vec(w$solution) < norm.vec(w.star))) {
      w.star <- w$solution
      b.star <- b
    }
  }
  if (is.null(w.star)) {
    stop("Constraints not satisfiable")
  }
  else if (norm.vec(w.star) > q/r) {
    stop("Bounding assumption of w violated")
  }

  # Return the model if we've made it this far
  return (function(x) {
    # x should be a vector of the proper size
    if (length(x) != length(w.star)) {
      stop("Pass a vector of valid length");
    }

    # Attempt to classify the given vector
    return (sign(dot.vec(w.star,x) - b.star))
  })
}

max.vector <- function(dframe) {
  norms <- apply(dframe, c(1), norm.vec)
  dframe.norms <- cbind(dframe, norms)
  dframe.norms <- dframe.norms[order(-dframe.norms[,ncol(dframe.norms)]),]
  dframe.norms[1,1:ncol(dframe.norms)-1]
}

norm.vec <- function(x) {
  sqrt(sum(x^2))
}

dot.vec <- function(v1,v2) {
  sum(v1 * v2)
}