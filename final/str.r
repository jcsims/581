> str(t.df)
'data.frame':	209 obs. of  8 variables:
 $ MYCT  : int  125 29 29 29 29 26 23 23 23 23 ...
 $ MMIN  : int  256 8000 8000 8000 8000 8000 16000 16000 16000 32000 ...
 $ MMAX  : int  6000 32000 32000 32000 16000 32000 32000 32000 64000 64000 ...
 $ CACHE : int  256 32 32 32 32 64 64 64 64 128 ...
 $ CHMIN : int  16 8 8 8 8 8 16 16 16 32 ...
 $ CHMAX : int  128 32 32 32 16 32 32 32 32 64 ...
 $ PRP   : int  198 269 220 172 132 318 367 489 636 1144 ...
 $ ERP   : int  199 253 253 253 132 290 381 381 749 1238 ...

> summary(t.df)
     MYCT             MMIN            MMAX           CACHE      
Min.   :  17.0   Min.   :   64   Min.   :   64   Min.   :  0.00 
1st Qu.:  50.0   1st Qu.:  768   1st Qu.: 4000   1st Qu.:  0.00 
Median : 110.0   Median : 2000   Median : 8000   Median :  8.00 
Mean   : 203.8   Mean   : 2868   Mean   :11796   Mean   : 25.21 
3rd Qu.: 225.0   3rd Qu.: 4000   3rd Qu.:16000   3rd Qu.: 32.00 
Max.   :1500.0   Max.   :32000   Max.   :64000   Max.   :256.00 

    CHMIN             CHMAX            PRP              ERP
Min.   : 0.000   Min.   :  0.00  Min.   :   6.0  Min.   :  15.00
1st Qu.: 1.000   1st Qu.:  5.00  1st Qu.:  27.0  1st Qu.:  28.00
Median : 2.000   Median :  8.00  Median :  50.0  Median :  45.00
Mean   : 4.699   Mean   : 18.27  Mean   : 105.6  Mean   :  99.33
3rd Qu.: 6.000   3rd Qu.: 24.00  3rd Qu.: 113.0  3rd Qu.: 101.00
Max.   :52.000   Max.   :176.00  Max.   :1150.0  Max.   :1238.00
