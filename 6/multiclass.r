# Implement a multi-class SVM using the binary SVM from the e1071 package,
# and pairwise classification. Implemented using the pseudocode in Knowledge
# Discovery with Support Vector Machines by Lutz Hamel, pg 190.

library('e1071') # svm()

# df is a data frame, with the class as the last column
# point is a single row data frame, with columns that match the original data
# frame
multi.class <- function(df, point) {
  # Assume that the clasification is the last column
  classes <- levels(df[[ncol(df)]])
  count <- rep(0,length(classes))
  for (i in 1:(length(count) - 1)) {
    for (j in (i+1):length(count)) {
      #Build a model for the two current categories
      dataset <- df[df[ncol(df)] == classes[i] | df[ncol(df)] == classes[j],]
      x <- dataset[-ncol(dataset)]
      y <- dataset[ncol(dataset)]
      model <- svm(x,y,type="C-classification")

      # Attempt to classify the point based on the current model
      classification  <- predict(model, point)
      if (classification == classes[i]) {
        count[i] = count[i] + 1
      }
      else {
        count[j] = count[j] + 1
      }
    }
  }
  # Return the class with the most positive matches
  max.count <- 0
  for (i in 1:length(count)) {
    if (count[i] > max.count) {
      max.label <- classes[i]
      max.count <- count[i]
    }
  }
  return (max.label)
}

# Returns a logical vector to compare the bitwise and library multiclass
# implementations
compare <- function(df) {
  # classify each point with the pairwise classification
  pairwise <- c()
  cols <- ncol(df)
  for (i in 1:(nrow(df))) {
    pred <- multi.class(df, df[i,-cols])
    pairwise <- append(pairwise, pred)
  }

  # Build the model and predict using the library function
  x <- df[-cols]
  y <- df[cols]
  model <- svm(x,y,type="C-classification")
  pred <- fitted(model)
  return (pairwise == pred)
}
