> str(t.df)
'data.frame':	712 obs. of  8 variables:
 $ survived: Factor w/ 2 levels "0","1": 1 2 2 2 1 1 1 2 2 2 ...
 $ pclass  : Factor w/ 3 levels "1","2","3": 3 1 3 1 3 1 3 3 2 3 ...
 $ sex     : Factor w/ 2 levels "female","male": 2 1 1 1 2 2 2 1 1 1 ...
 $ age     : num  22 38 26 35 35 54 2 27 14 4 ...
 $ sibsp   : int  1 1 0 1 0 0 3 0 1 1 ...
 $ parch   : int  0 0 0 0 0 0 1 2 0 1 ...
 $ fare    : num  7.25 71.28 7.92 53.1 8.05 ...
 $ embarked: Factor w/ 3 levels "C","Q","S": 3 1 3 3 3 3 3 3 1 3 ...

> summary(t.df)
survived pclass      sex           age            sibsp       
 0:424    1:184   female:259   Min.   : 0.42   Min.   :0.000  
 1:288    2:173   male  :453   1st Qu.:20.00   1st Qu.:0.000  
          3:355                Median :28.00   Median :0.000  
                               Mean   :29.64   Mean   :0.514  
                               3rd Qu.:38.00   3rd Qu.:1.000  
                               Max.   :80.00   Max.   :5.000  

    fare           embarked       parch
Min.   :  0.00      C:130      Min.   :0.0000
1st Qu.:  8.05      Q: 28      1st Qu.:0.0000
Median : 15.65      S:554      Median :0.0000
Mean   : 34.57                 Mean   :0.4326
3rd Qu.: 33.00                 3rd Qu.:1.0000
Max.   :512.33                 Max.   :6.0000
