# This function defines a simple learning algorithm:
# It returns a function that is able to classify a given point
# based in the initial training data set
simple.learning <- function(dframe) {
  # Simple sanity check for the passed data
  if (!is.data.frame(dframe)) {
    stop("This functions accepts only data frames.")
  }

  # Compute average point for positive observations
  pos <- dframe[dframe[ncol(dframe)] == 1,]
  pos.means <- colMeans(pos)

  # Compute average point for negative observations
  neg <- dframe[dframe[ncol(dframe)] == -1,]
  neg.means <- colMeans(neg)


  # Get d, the difference vector between the two classification groups
  d <- pos.means - neg.means
  d <- d[1:(length(d)-1)]

  # Get c, the average of the two classification groups
  c.vec <- 0.5*(pos.means + neg.means)
  c.vec <- c.vec[1:(length(c.vec)-1)]


  # Build the model
  return (function(x) {
    # x should be a vector of the proper size
    if (length(x) != length(d)) {
      stop("Pass a vector of valid length");
    }

    # Attempt to classify the given vector
    # This (and the above calculations) are taken from the text,
    # Knowledge Discovery with Support Vector Machines, pp54-56
    return (sign(sum((x - c.vec) * d)))
  })
}
