> tasers <- read.csv("tasers.csv")
> tasers
  Voltage Charges     Type Deadly
1   50000       1  contact  false
2   10000      10      ced   true
3   25000       4  contact   true
4    5000      25      ced   true
5   50000       4      ced   true
> t.model <- majModel(tasers)
> t.model(c(20000,4,"ced"))
[1] " true"
> mammals <- read.csv("mammals.csv")
> mammals
  Legs Wings  Fur Feathers Mammal
1    4    no  yes       no   true
2    2   yes   no      yes  false
3    4    no   no       no  false
4    4   yes  yes       no   true
5    3    no   no       no  false
> m.model <- majModel(mammals)
> m.model(c(3,"yes","yes","no"))
[1] " false"
