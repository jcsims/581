# Compute a simple majority label model
# Returns a function that accepts a vector of independent attributes
majModel <- function(df) {
  # process the data frame, getting the dependent column
  dep <- ncol(df)
  dep.levels  <- levels(df[[dep]])

  # Determine which value is the majority
  maj <- c(-1,"") #Start with a default empty value
  for (level in dep.levels) {
    level.rows <- nrow(subset(df, df[dep] == level))
    if (maj[1] < level.rows) {
      maj <- c(level.rows, level)
    }
  }
  # Build the majority function
  # This simple function just returns the precomputed majority label
  return (function (a) { 
      # Check for valid input
      # We would expect a vector with our full complement of
      # independent attributes (but currently only check for length)
      if (length(a) != dep -1) {
        stop("Pass a vector of valid length")
      }
      return(maj[2])
  })
}
