# Functions to process the titanic dataset
#

library('e1071')
# Load the dataset, preparing it in the process
loadData <- function() {
  t.df <- read.csv("machine.data")
  t.df <- prepare(t.df)
  return (t.df)
}

# Ensure that factors are set up properly, and strip the columns we're not using
# The svm models don't fit using any observation with an NA, so remove those as
# well
prepare  <- function(t.df) {
  t.df <- subset(t.df, select = -c(Model))
  # t.df <- na.omit(t.df)
  return(t.df)
}

# Build a model given parameters
# Linear wasn't used in final bootstrapping
buildModel <- function(t.df, kernel, cost, degree, gamma) {
  if (kernel == "polynomial") {
    model <- buildPoly(t.df,cost,degree)
    return (model)
  }
  else {
    model <- buildRadial(t.df,cost,gamma)
    return (model)
  }
}

buildLinear <- function(t.df,c) {
  model <- svm(ERP ~ .,
               data = t.df,
               type="eps-regression",
               kernel="linear",
               cost=c,
               cross=10)
  return(model)
}

buildPoly <- function(t.df,c,d) {
  model <- svm(ERP ~ .,
               data = t.df,
               type="eps-regression",
               kernel="polynomial",
               degree=d,
               cost=c,
               cross=10)
  return(model)
}

buildRadial <- function(t.df,c,g = 1/ncol(t.df)) {
  model <- svm(ERP ~ .,
               data = t.df,
               type="eps-regression",
               kernel="radial",
               gamma=g,
               cost=c)
  return(model)
}

# Simple function to compute the mean squared error across the dataset
computeLoss <- function(original, predicted) {
  elements <- length(original)
  sum <- 0
  for (i in 1:elements) {
    sum <- sum + (original[i] - predicted[i])^2
  }
  return (sum / elements) 
}

# Build a bootstrap for an optimal set of parameters
# Uses 200 samples, returns the sorted list of errors
bootstrap <- function(t.df,kernel, cost, degree = 0, gamma = 0) {
  num.elements <- nrow(t.df)
  errors <- c()
  for (i in 1:200) {
    current.sample <- t.df[sample(1:num.elements, num.elements, replace = TRUE),]
    current.model <- buildModel(current.sample, kernel, cost, degree, gamma)
    current.pred <- fitted(current.model)
    errors <- append(errors, computeLoss(current.sample$ERP, current.pred))
  }
  return (errors[sort.list(errors)])
}
