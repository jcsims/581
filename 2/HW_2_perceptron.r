# Returns a function that can be used to classify an observation
# Based on the perceptron model, algorithm details taken from
# Knowledge Discovery with Support Vector Machines, pp 62-66
per.model <- function(dframe, n) {
  # Simple sanity check for the passed data
  if (!is.data.frame(dframe)) {
    stop("This functions accepts only data frames.")
  }

  w <- rep(0,ncol(dframe)-1)
  b <- 0
  r <- norm.vec(max.vector(dframe[,1:ncol(dframe)-1]))

  # Now loop over the training data set and refine w and b
  repeat {
    for (i in 1:nrow(dframe)) {
      x <- dframe[i,1:ncol(dframe)-1]
      y <- dframe[i,ncol(dframe)]
      if( sign( dot.vec(w,x) - b)[1] != y) {
        w <- w + (n * y * x)
        b <- b - (n * y * r^2)
        complete <- FALSE
      }
    }
    if(complete) {
      break
    }
    else {
      complete <- TRUE
    }
  }

  return (function(x) {
    # x should be a vector of the proper size
    if (length(x) != length(w)) {
      stop("Pass a vector of valid length");
    }

    # Attempt to classify the given vector
    return (sign(dot.vec(w,x) - b))
  })
}

max.vector <- function(dframe) {
  norms <- apply(dframe, c(1), norm.vec)
  dframe.norms <- cbind(dframe, norms)
  dframe.norms <- dframe.norms[order(-dframe.norms[,ncol(dframe.norms)]),]
  dframe.norms[1,1:ncol(dframe.norms)-1]
}

norm.vec <- function(x) {
  sqrt(sum(x^2))
}

dot.vec <- function(v1,v2) {
  sum(v1 * v2)
}
